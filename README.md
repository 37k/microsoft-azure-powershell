# ![AzureIcon] ![PowershellIcon] Microsoft Azure PowerShell ** edition version 55Sp

This repository contains PowerShell cmdlets for developers and administrators to develop, deploy, and manage Microsoft Azure applications.

Try it out in Azure Cloud Shell!
